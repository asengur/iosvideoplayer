import UIKit

struct Videos {
    
    var videos: [String]
    var videosInfo: [String]
    var images: [String]
    
    init(videos: [String], videosInfo: [String], images: [String]) {
        self.videos = videos
        self.videosInfo = videosInfo
        self.images = images
    }
    
}


class Deneme {
    
    var deneme = Videos(videos: <#T##[String]#>, videosInfo: <#T##[String]#>, images: <#T##[String]#>)
    
}
