//
//  Video.swift
//  VideoPlayer
//
//  Created by Ali Şengür on 23.01.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import Foundation

struct Video {
    
    var videos: [String]
    let videosInfo: [String]
    let imagesURLS: [String]
    
        
    init() {
        self.videos = ["video1","video2","video3","video4"]
        
        self.videosInfo = ["video1-info","video2-info","video3-info","video4-info"]
        self.imagesURLS = ["https://cdn.pixabay.com/photo/2020/01/14/21/48/norway-4766392_960_720.jpg",           "https://cdn.pixabay.com/photo/2020/01/03/14/26/scotland-4738257_960_720.jpg", "https://cdn.pixabay.com/photo/2020/01/19/10/38/landscape-4777329_960_720.jpg", "https://cdn.pixabay.com/photo/2020/01/20/12/41/namibia-4780251_960_720.jpg"]
    }
    
}

