//
//  VideoPlayerTableViewCell.swift
//  VideoPlayer
//
//  Created by Ali Şengür on 22.01.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit
import Kingfisher

protocol VideoCellDelegate {
    func didTapWatchButton(index: Int)
    func didTapInfoButton(index: Int)
}


class VideoPlayerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var watchButton: UIButton!
    @IBOutlet weak var infoButton: UIButton!
    
    
    var delegate: VideoCellDelegate?
    var index: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func cellInit(imageURL: String, title: String, subTitle: String) {
     
        let url = URL(string: imageURL)
        cellImageView.kf.setImage(with: url)
        titleLabel.text = title
        subtitleLabel.text = subTitle
    }
    

    @IBAction func watchButtonClicked(_ sender: UIButton) {
         delegate?.didTapWatchButton(index: (index!.row))
    }
    
    
    
    @IBAction func infoButtonClicked(_ sender: UIButton) {
        delegate?.didTapInfoButton(index: (index!.row))

    }
    
    
}
