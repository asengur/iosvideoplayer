//
//  ViewController.swift
//  VideoPlayer
//
//  Created by Ali Şengür on 15.01.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit


class VideoPlayerViewController: UIViewController {

    @IBOutlet weak var videoView: UIView!
    @IBOutlet private weak var pausePlayButton: UIButton!
    @IBOutlet weak var leftTimeLabel: UILabel!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    
    
    //MARK: -Properties
    var queuePlayer: AVQueuePlayer!   // created AVQueuePlayer object
    var playerLayer: AVPlayerLayer!   // AVPlayerLayer object is created.
    var isActive: Bool = true   // this controls player state whether it is paused or playing
    var isPlaying: Bool {
        return queuePlayer.rate != 0 && queuePlayer.error == nil
    }
    var videos: [String] = []
    var video: String!
    private var playerItems: [AVPlayerItem] = []
    private var playerItem: AVPlayerItem?
    private var tempIndex = 0
    private var token: NSKeyValueObservation?
    
    
    
    let videoSlider: UISlider = {
        let slider = UISlider()
        slider.translatesAutoresizingMaskIntoConstraints = false
        slider.minimumTrackTintColor = UIColor.red
        slider.maximumTrackTintColor = UIColor.white
        slider.setThumbImage(UIImage(named: "red-circle"), for: UIControl.State.normal)
        
        slider.addTarget(self, action: #selector(sliderChanged), for: UIControl.Event.valueChanged)
        return slider
    }()
    
    
    
    //MARK: -Functions
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startVideos(videos: videos)
        let tapView = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        videoView.addGestureRecognizer(tapView)
        videoPlayerInit()  // call the videoPlayerInit function.
        self.tempIndex = self.playerItems.endIndex-1
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.landscapeRight, andRotateTo: UIInterfaceOrientation.landscapeRight)
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
    }
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let pl = playerLayer {
            pl.frame = videoView.bounds
        }
    }
    
    
    
    
    
    @objc func sliderChanged() {
        print(videoSlider.value)
        if let duration = queuePlayer.currentItem?.duration {
            let totalSeconds = CMTimeGetSeconds(duration)
            let value = Float64(videoSlider.value) * totalSeconds
            let seekTime = CMTime(value: Int64(value), timescale: 1)
            queuePlayer.seek(to: seekTime, completionHandler: {(completedSeek)
                in
            })
        }
    }

    
    
    func videoPlayerInit() {
        pausePlayButton.isHidden = true
        videoSlider.isHidden = true
        leftTimeLabel.isHidden = true
        previousButton.isHidden = true
        nextButton.isHidden = true
        videoView.addSubview(pausePlayButton)
        videoView.addSubview(videoSlider)
        videoView.addSubview(leftTimeLabel)
        videoView.addSubview(previousButton)
        videoView.addSubview(nextButton)
        
        videoSlider.rightAnchor.constraint(equalTo: videoView.rightAnchor, constant: -10).isActive = true
        videoSlider.leftAnchor.constraint(equalTo: leftTimeLabel.rightAnchor, constant: 10).isActive = true
        videoSlider.bottomAnchor.constraint(equalTo: videoView.bottomAnchor, constant: -20).isActive = true
        videoSlider.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }
    

    
    //MARK: -User Functions
    @IBAction func pausePlayButtonClicked(_ sender: UIButton) {
        updateUI()
    }
    
    
    @IBAction func previousButtonClicked(_ sender: UIButton) {
        
        for _ in 0..<videos.count-1 {
            queuePlayer.advanceToNextItem()
        }
    }
    

    
    @IBAction func nextButtonClicked(_ sender: UIButton) {
    
        self.queuePlayer?.advanceToNextItem()
    }
    
    
    @objc func viewTapped() {
        pausePlayButton.isHidden = !isActive
        videoSlider.isHidden = !isActive
        //videoLengthLabel.isHidden = !isTapped
        leftTimeLabel.isHidden = !isActive
        previousButton.isHidden = !isActive
        nextButton.isHidden = !isActive
        isActive = !isActive
        
    }
    
    
    func updateUI() {  // when tapped pausePlayButton, updates the UI
        if isPlaying {
            queuePlayer.pause()
            pausePlayButton.setImage(UIImage(named: "play"), for: .normal)
        } else {
            queuePlayer.play()
            pausePlayButton.setImage(UIImage(named: "pause"), for: .normal)
        }
    }
    
    
    
    func startVideos(videos: [String]) {
        
        queuePlayer = AVQueuePlayer()
        playerLayer = AVPlayerLayer(player: queuePlayer)

        
        guard let playerLayer = playerLayer else { fatalError("Error creating player layer") }
        playerLayer.frame = view.layer.bounds
        playerLayer.videoGravity = .resize
        videoView.layer.addSublayer(playerLayer)  // add playerLayer object to videoView's layer
        
        addVideos(videos: self.videos)
        
        
        let interval = CMTime(value: 1, timescale: 2)
        queuePlayer.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: {(progressTime)
            in
            let seconds = CMTimeGetSeconds(progressTime)
            let secondsString = String(format: "%02d", Int(seconds) % 60)
            let minutesString = String(format: "%02d", Int(seconds) / 60)
            self.leftTimeLabel.text = "\(minutesString):\(secondsString)"

            if let duration = self.queuePlayer.currentItem?.duration {
                let durationSeconds = CMTimeGetSeconds(duration)
                self.videoSlider.value = Float(seconds / durationSeconds)
            }
            
        })
        
        self.queuePlayer.play()
        
        self.token = self.queuePlayer!.observe(\.currentItem) { [weak self] player, _ in
            if self!.queuePlayer.items().count == 1 {
                self?.addVideos(videos: (self?.videos)!)  
            }
            
        }
    }
    
    
    
    func addVideos(videos: [String]) {
        for video in videos {
            guard let path = Bundle.main.path(forResource: "\(video)", ofType: "mp4") else { return }
            let url = URL(fileURLWithPath: path)
            let asset = AVAsset(url: url)
            let item = AVPlayerItem(asset: asset)
            
            queuePlayer.insert(item, after: queuePlayer.items().last)
        }
        
        
    }

}
