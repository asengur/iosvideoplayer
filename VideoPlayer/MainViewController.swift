//
//  MainViewController.swift
//  VideoPlayer
//
//  Created by Ali Şengür on 15.01.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit
import Kingfisher

class MainViewController: UIViewController{
        
    
    
    @IBOutlet weak var videoTableView: UITableView!
    
    // Created instance from Video struct
    var data = Video() {
        didSet {
            if self.videoTableView != nil {
                self.videoTableView.reloadData()
            }
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        videoTableView.allowsSelection = true
        setTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    

    fileprivate func setTableView() {
        let videoTableViewCellNib = UINib(nibName: "VideoPlayerTableViewCell", bundle: nil)
        videoTableView.delegate = self
        videoTableView.dataSource = self
        videoTableView.register(videoTableViewCellNib, forCellReuseIdentifier: "videoTableViewCell")
        
    }
    
}


//MARK: -Table View Functions
extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.videos.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = videoTableView.dequeueReusableCell(withIdentifier: "videoTableViewCell", for: indexPath) as! VideoPlayerTableViewCell
        
        cell.cellInit(imageURL: data.imagesURLS[indexPath.item], title: data.videos[indexPath.item], subTitle: data.videosInfo[indexPath.item])
        cell.delegate = self
        cell.index = indexPath
        return cell
    }
    
}


//MARK: -MainViewController
extension MainViewController: VideoCellDelegate {
    
    func didTapWatchButton(index: Int) {
        let selectedVideo = data.videos[index]
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        if let vc = mainStoryBoard.instantiateViewController(identifier: "VideoPlayerViewController") as? VideoPlayerViewController {
            
            vc.video = selectedVideo
            var newRow = index
            for _ in 0..<self.data.videos.count {
                if newRow >= data.videos.count {
                    newRow = 0
                }
                vc.videos.append(self.data.videos[newRow])
                newRow += 1
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    
    func didTapInfoButton(index: Int) {
        let selectedVideo = self.data.videos[index]
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        if let vc = mainStoryBoard.instantiateViewController(identifier: "InfoViewController") as? InfoViewController {
            vc.titleLabel = selectedVideo
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
