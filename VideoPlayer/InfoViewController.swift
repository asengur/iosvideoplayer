//
//  InfoViewController.swift
//  VideoPlayer
//
//  Created by Ali Şengür on 24.01.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController {

    @IBOutlet weak var infoTitleLabel: UILabel!
    @IBOutlet weak var infoTextView: UITextView!
    
    var titleLabel: String!
    var textView: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setInfoViewController()
    }
    
    func setInfoViewController() {
        infoTitleLabel.text = titleLabel
    }
    
    


}
